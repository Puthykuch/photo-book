package com.puthykuch.photobook.service.respository.storage.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.databinding.BaseObservable;
import androidx.recyclerview.widget.DiffUtil;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "photos")
public class Photo extends BaseObservable implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    @Expose
    @SerializedName(value = "id")
    private Integer id;

    @ColumnInfo(name = "author")
    @SerializedName(value = "author")
    @Expose
    private String author;

    @ColumnInfo(name = "width")
    @SerializedName(value = "width")
    @Expose
    private Long width;

    @Override
    public String toString() {
        return "Photo{" +
                "id=" + id +
                ", author='" + author + '\'' +
                ", width=" + width +
                ", height=" + height +
                ", url='" + url + '\'' +
                ", download_url='" + downloadUrl + '\'' +
                ", mark=" + mark +
                '}';
    }

    @ColumnInfo(name = "height")
    @SerializedName(value = "height")
    @Expose
    private Long height;

    @ColumnInfo(name = "url")
    @SerializedName(value = "url")
    @Expose
    private String url;

    @ColumnInfo(name = "download_url")
    @SerializedName(value = "download_url")
    @Expose
    private String downloadUrl;

    public boolean mark = false;

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Long getWidth() {
        return width;
    }

    public void setWidth(Long width) {
        this.width = width;
    }

    public Long getHeight() {
        return height;
    }

    public void setHeight(Long height) {
        this.height = height;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public boolean getMark() {
        return mark;
    }

    public void setMark(boolean mark) {
        this.mark = mark;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.author);
        dest.writeValue(this.width);
        dest.writeValue(this.height);
        dest.writeString(this.url);
        dest.writeString(this.downloadUrl);
        dest.writeValue(this.mark);
    }

    public Photo() {
    }

    protected Photo(Parcel in) {
        this.id = in.readInt();
        this.author = in.readString();
        this.width = (Long) in.readValue(Long.class.getClassLoader());
        this.height = (Long) in.readValue(Long.class.getClassLoader());
        this.url = in.readString();
        this.downloadUrl = in.readString();
        this.mark = (boolean) in.readValue(boolean.class.getClassLoader());
    }

    public static final Parcelable.Creator<Photo> CREATOR = new Parcelable.Creator<Photo>() {
        @Override
        public String toString() {
            return "$classname{}";
        }

        @Override
        public Photo createFromParcel(Parcel source) {
            return new Photo(source);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    // use for ordering the items in view
    public static DiffUtil.ItemCallback<Photo> DIFF_CALLBACK = new DiffUtil.ItemCallback<Photo>() {
        @Override
        public boolean areItemsTheSame(@NonNull Photo oldItem, @NonNull Photo newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Photo oldItem, @NonNull Photo newItem) {
            return oldItem.getId().equals(newItem.getId());
        }
    };
}
