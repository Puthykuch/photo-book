package com.puthykuch.photobook.service.respository.storage.ado;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.puthykuch.photobook.service.respository.storage.models.Photo;

import java.util.List;

@Dao
public interface PhotoBookmarksDao {

    @Query("SELECT * FROM photos WHERE mark = 1")
    List<Photo> getAllPhotoBookmark();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertPhoto(Photo photo);

    @Delete
    void delete(Photo photo);

    @Update
    void update(Photo photo);
}