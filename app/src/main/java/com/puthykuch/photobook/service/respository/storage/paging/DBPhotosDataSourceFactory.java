package com.puthykuch.photobook.service.respository.storage.paging;


import androidx.paging.DataSource;

import com.puthykuch.photobook.service.respository.storage.ado.PhotosDao;

public class DBPhotosDataSourceFactory extends DataSource.Factory {

    private DBPhotosPageKeyedDataSource photosPageKeyedDataSource;

    public DBPhotosDataSourceFactory(PhotosDao dao) {
        photosPageKeyedDataSource = new DBPhotosPageKeyedDataSource(dao);
    }

    @Override
    public DataSource create() {
        return photosPageKeyedDataSource;
    }

}
