package com.puthykuch.photobook.service.respository.network;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import com.puthykuch.photobook.service.respository.network.paging.NetPhotosDataSourceFactory;
import com.puthykuch.photobook.service.respository.network.paging.NetPhotosPageKeyedDataSource;
import com.puthykuch.photobook.service.respository.storage.models.NetworkState;
import com.puthykuch.photobook.service.respository.storage.models.Photo;
import com.puthykuch.photobook.ui.AppConstants;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class PhotosNetwork implements AppConstants {
    final private LiveData<PagedList<Photo>> photosPaged;
    final private LiveData<NetworkState> networkState;

    public PhotosNetwork(NetPhotosDataSourceFactory dataSourceFactory, PagedList.BoundaryCallback<Photo> boundaryCallback){
        PagedList.Config pagedListConfig = (new PagedList.Config.Builder())
                .setEnablePlaceholders(false)
                .setInitialLoadSizeHint(LOADING_PAGE_SIZE)
                .setPageSize(LOADING_PAGE_SIZE).build();

        networkState = Transformations.switchMap(dataSourceFactory.getNetworkStatus(),
                (Function<NetPhotosPageKeyedDataSource, LiveData<NetworkState>>)
                        NetPhotosPageKeyedDataSource::getNetworkState);

        Executor executor = Executors.newFixedThreadPool(NUMBERS_OF_THREADS);

        LivePagedListBuilder livePagedListBuilder = new LivePagedListBuilder(dataSourceFactory, pagedListConfig);
        photosPaged = livePagedListBuilder.setFetchExecutor(executor).setBoundaryCallback(boundaryCallback).build();
    }


    public LiveData<PagedList<Photo>> getPagedPhotos(){
        return photosPaged;
    }

    public LiveData<NetworkState> getNetworkState() {
        return networkState;
    }

}
