package com.puthykuch.photobook.service.respository.network.paging;


import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

import com.puthykuch.photobook.service.respository.network.ApiService;
import com.puthykuch.photobook.service.respository.storage.models.Photo;

import io.reactivex.subjects.ReplaySubject;

public class NetPhotosDataSourceFactory extends DataSource.Factory {

    private MutableLiveData<NetPhotosPageKeyedDataSource> networkStatus;
    private NetPhotosPageKeyedDataSource moviesPageKeyedDataSource;

    public NetPhotosDataSourceFactory(ApiService mApiService) {
        this.networkStatus = new MutableLiveData<>();
        moviesPageKeyedDataSource = new NetPhotosPageKeyedDataSource(mApiService);
    }

    @Override
    public DataSource create() {
        networkStatus.postValue(moviesPageKeyedDataSource);
        return moviesPageKeyedDataSource;
    }

    public MutableLiveData<NetPhotosPageKeyedDataSource> getNetworkStatus() {
        return networkStatus;
    }

    public ReplaySubject<Photo> getPhotos() {
        return moviesPageKeyedDataSource.getPhotos();
    }

}
