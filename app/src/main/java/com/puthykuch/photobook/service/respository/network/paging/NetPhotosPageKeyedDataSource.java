package com.puthykuch.photobook.service.respository.network.paging;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.PageKeyedDataSource;

import com.puthykuch.photobook.service.respository.network.ApiService;
import com.puthykuch.photobook.service.respository.storage.models.NetworkState;
import com.puthykuch.photobook.service.respository.storage.models.Photo;
import com.puthykuch.photobook.ui.AppConstants;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.subjects.ReplaySubject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class NetPhotosPageKeyedDataSource extends PageKeyedDataSource<String, Photo> implements AppConstants {
    private final ApiService mApiService;
    private final MutableLiveData networkState;
    private final ReplaySubject<Photo> photosObservable;

    NetPhotosPageKeyedDataSource(ApiService apiService) {
        mApiService = apiService;

        networkState = new MutableLiveData();
        photosObservable = ReplaySubject.create();
    }

    public MutableLiveData getNetworkState() {
        return networkState;
    }

    public ReplaySubject<Photo> getPhotos() {
        return photosObservable;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<String> params, @NonNull final LoadInitialCallback<String, Photo> callback) {
        Timber.d("Loading Initial Rang, Count %s", params.requestedLoadSize);

        networkState.postValue(NetworkState.LOADING);

        Call<List<Photo>> callBack;
        callBack = mApiService.getPhotos(1, AppConstants.LOADING_PAGE_SIZE);
        callBack.enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(@NotNull Call<List<Photo>> call, @NotNull Response<List<Photo>> response) {
                if (response.isSuccessful()) {

                    assert response.body() != null;
                    callback.onResult(response.body(), Integer.toString(1), Integer.toString(2));

                    networkState.postValue(NetworkState.LOADED);

                    for (Photo photo : response.body()) {
                        photosObservable.onNext(photo);
                    }
                } else {
                    Timber.d("API CALL %s", response.message());

                    networkState.postValue(new NetworkState(NetworkState.Status.FAILED, response.message()));
                }
            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {
                String errorMessage;

                if (t.getMessage() == null) {
                    errorMessage = "unknown error";
                } else {
                    errorMessage = t.getMessage();
                }

                networkState.postValue(new NetworkState(NetworkState.Status.FAILED, errorMessage));

                callback.onResult(new ArrayList<>(), Integer.toString(1), Integer.toString(2));
            }
        });
    }


    @Override
    public void loadAfter(@NonNull LoadParams<String> params, final @NonNull LoadCallback<String, Photo> callback) {
        Timber.d("Loading page %s", params.key);

        networkState.postValue(NetworkState.LOADING);

        final AtomicInteger page = new AtomicInteger(0);
        try {
            page.set(Integer.parseInt(params.key));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        Call<List<Photo>> callBack = mApiService.getPhotos(page.get(), LOADING_PAGE_SIZE);
        callBack.enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(@NotNull Call<List<Photo>> call, @NotNull Response<List<Photo>> response) {
                if (response.isSuccessful()) {

                    assert response.body() != null;
                    callback.onResult(response.body(), Integer.toString(page.get() + 1));

                    networkState.postValue(NetworkState.LOADED);

                    for (Photo photo : response.body()) {
                        photosObservable.onNext(photo);
                    }

                } else {
                    networkState.postValue(new NetworkState(NetworkState.Status.FAILED, response.message()));
                    Timber.d("API CALL %s", response.message());
                }
            }

            @Override
            public void onFailure(@NotNull Call<List<Photo>> call, Throwable t) {
                String errorMessage;

                if (t.getMessage() == null) {
                    errorMessage = "unknown error";
                } else {
                    errorMessage = t.getMessage();
                }

                networkState.postValue(new NetworkState(NetworkState.Status.FAILED, errorMessage));
                callback.onResult(new ArrayList<>(), Integer.toString(page.get()));
            }
        });
    }


    @Override
    public void loadBefore(@NonNull LoadParams<String> params, @NonNull LoadCallback<String, Photo> callback) {

    }
}
