package com.puthykuch.photobook.service.respository.storage.paging;


import androidx.paging.DataSource;

import com.puthykuch.photobook.service.respository.storage.ado.PhotosDao;

public class DBPhotoBookmarksDataSourceFactory extends DataSource.Factory {

    private DBPhotoBookmarksPageKeyedDataSource photoBookmarksPageKeyedDataSource;

    public DBPhotoBookmarksDataSourceFactory(PhotosDao dao) {
        photoBookmarksPageKeyedDataSource = new DBPhotoBookmarksPageKeyedDataSource(dao);
    }

    @Override
    public DataSource create() {
        return photoBookmarksPageKeyedDataSource;
    }

}
