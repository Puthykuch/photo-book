package com.puthykuch.photobook.service.respository.storage;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.puthykuch.photobook.service.respository.storage.ado.PhotoBookmarksDao;
import com.puthykuch.photobook.service.respository.storage.ado.PhotosDao;
import com.puthykuch.photobook.service.respository.storage.models.Photo;
import com.puthykuch.photobook.service.respository.storage.paging.DBPhotosDataSourceFactory;
import com.puthykuch.photobook.ui.AppConstants;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@Database(entities = {Photo.class}, version = 1)
public abstract class PhotoBookDatabase extends RoomDatabase implements AppConstants {

    private static PhotoBookDatabase instance;

    public abstract PhotosDao photosDao();
    public abstract PhotoBookmarksDao photoBookmarksDao();

    private static final Object sLock = new Object();

    private LiveData<PagedList<Photo>> photosPaged;
//    private LiveData<PagedList<Photo>> photoBookmarksPaged;

    public static PhotoBookDatabase getInstance(Context context) {
        synchronized (sLock) {
            if (instance == null) {
                instance = Room.databaseBuilder(context.getApplicationContext(),
                        PhotoBookDatabase.class, DATA_BASE_NAME)
                        .build();

                instance.init();
            }
            return instance;
        }
    }

    private void init() {
        PagedList.Config pagedListConfig = (new PagedList.Config.Builder()).setEnablePlaceholders(false)
                .setInitialLoadSizeHint(Integer.MAX_VALUE).setPageSize(Integer.MAX_VALUE).build();

        Executor executor = Executors.newFixedThreadPool(NUMBERS_OF_THREADS);

        DBPhotosDataSourceFactory dbPhotosDataSourceFactory = new DBPhotosDataSourceFactory(photosDao());
//        DBPhotoBookmarksDataSourceFactory dbPhotoBookmarksDataSourceFactory = new DBPhotoBookmarksDataSourceFactory(photosDao());

        LivePagedListBuilder livePhotoPagedListBuilder = new LivePagedListBuilder(dbPhotosDataSourceFactory, pagedListConfig);
        photosPaged = livePhotoPagedListBuilder.setFetchExecutor(executor).build();

//        LivePagedListBuilder liveBookmarkPagedListBuilder = new LivePagedListBuilder(dbPhotoBookmarksDataSourceFactory, pagedListConfig);
//        photoBookmarksPaged = liveBookmarkPagedListBuilder.setFetchExecutor(executor).build();
    }

    public LiveData<PagedList<Photo>> getPhotosPaged() {
        return photosPaged;
    }

//    public LiveData<PagedList<Photo>> getPhotoBookmarksPaged() {
//        return photoBookmarksPaged;
//    }
}
