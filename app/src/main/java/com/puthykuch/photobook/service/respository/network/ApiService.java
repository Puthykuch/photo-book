package com.puthykuch.photobook.service.respository.network;

import com.puthykuch.photobook.service.respository.storage.models.Photo;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("v2/list")
    Call<List<Photo>> getPhotos(@Query("page") int page, @Query("limit") int limit);

    @GET("id/{imageId}/{width}/{height}")
    Call<ResponseBody> getFullSizeImage(@Path("imageId") int imageId, @Path("width") int width, @Path("height") int height);

}
