package com.puthykuch.photobook.service.respository.storage.paging;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.puthykuch.photobook.service.respository.storage.ado.PhotosDao;
import com.puthykuch.photobook.service.respository.storage.models.Photo;

import java.util.List;

import timber.log.Timber;

public class DBPhotoBookmarksPageKeyedDataSource extends PageKeyedDataSource<String, Photo> {

    private final PhotosDao photoDao;

    public DBPhotoBookmarksPageKeyedDataSource(PhotosDao dao) {
        photoDao = dao;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<String> params, @NonNull final LoadInitialCallback<String, Photo> callback) {
        Timber.i("Loading Initial Rang, Count " + params.requestedLoadSize);

        List<Photo> photos = photoDao.getAllPhoto();
        if (photos.size() != 0) {
            callback.onResult(photos, "0", "1");
        }
    }

    @Override
    public void loadAfter(@NonNull LoadParams<String> params, final @NonNull LoadCallback<String, Photo> callback) {
    }

    @Override
    public void loadBefore(@NonNull LoadParams<String> params, @NonNull LoadCallback<String, Photo> callback) {
    }
}
