package com.puthykuch.photobook.service.respository;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.paging.PagedList;

import com.puthykuch.photobook.service.respository.network.ApiService;
import com.puthykuch.photobook.service.respository.network.PhotosNetwork;
import com.puthykuch.photobook.service.respository.network.paging.NetPhotosDataSourceFactory;
import com.puthykuch.photobook.service.respository.storage.PhotoBookDatabase;
import com.puthykuch.photobook.service.respository.storage.ado.PhotosDao;
import com.puthykuch.photobook.service.respository.storage.models.NetworkState;
import com.puthykuch.photobook.service.respository.storage.models.Photo;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class PhotosRepository {

    private static PhotosRepository photosRepository;

    static Context mContext;
    static ApiService mApiService;

    final private PhotosNetwork network;
    final private PhotoBookDatabase database;

    final private MediatorLiveData photosLiveDataMerger;
    final private MutableLiveData<List<Photo>> mutableLiveData = new MutableLiveData<>();

    public PhotosRepository(Context context, ApiService mApiService) {
        NetPhotosDataSourceFactory dataSourceFactory = new NetPhotosDataSourceFactory(mApiService);
        network = new PhotosNetwork(dataSourceFactory, boundaryCallback);
        database = PhotoBookDatabase.getInstance(context.getApplicationContext());

        // when we get new photos from net we set them into the database
        photosLiveDataMerger = new MediatorLiveData<>();
        photosLiveDataMerger.addSource(network.getPagedPhotos(), value -> {
            photosLiveDataMerger.setValue(value);
            Timber.d("photos: " + value.toString());
        });

        // save the photos into db
        dataSourceFactory.getPhotos().
                observeOn(Schedulers.io()).
                subscribe(photo -> {
                    database.photosDao().insertPhoto(photo);
                });

        new GetPhotoBookmarkTask(mContext, database, mutableLiveData).execute();
    }

    private PagedList.BoundaryCallback<Photo> boundaryCallback = new PagedList.BoundaryCallback<Photo>() {
        @Override
        public void onZeroItemsLoaded() {
            super.onZeroItemsLoaded();
            photosLiveDataMerger.addSource(database.getPhotosPaged(), value -> {
                photosLiveDataMerger.setValue(value);
                photosLiveDataMerger.removeSource(database.getPhotosPaged());
            });
        }
    };

    public static PhotosRepository getInstance(Context context, ApiService apiService) {
        if (photosRepository == null) {
            mContext = context;
            mApiService = apiService;
            photosRepository = new PhotosRepository(context, mApiService);
        }

        return photosRepository;
    }

    public LiveData<PagedList<Photo>> getPhotos() {
        return photosLiveDataMerger;
    }

    public LiveData<List<Photo>> getBookMarks() {
        MutableLiveData<List<Photo>> newLiveData = new MutableLiveData<>();

        List<Photo> bookMarks = new ArrayList<>();

        List<Photo> photos = (List<Photo>) photosLiveDataMerger.getValue();
        for(Photo photo: photos) {
            if(photo.getMark()) {
                bookMarks.add(photo);
            }
        }

        newLiveData.setValue(bookMarks);

        return newLiveData;
    }

    public LiveData<NetworkState> getNetworkState() {
        return network.getNetworkState();
    }

    public void toggleBookMark(Photo photo) {
        if (photo.getMark()) {
            photo.setMark(false);
        } else {
            photo.setMark(true);
        }

        new UpdatePhotoTask(mContext, database, photo).execute();
    }

    public void removeBookMark(Photo photo) {
        photo.setMark(false);
        new UpdatePhotoTask(mContext, database, photo).execute();
    }

    private static class UpdatePhotoTask extends AsyncTask<Void, Void, Boolean> {
        private Context mContext;
        private PhotoBookDatabase mDatabase;
        private Photo mPhoto;

        public UpdatePhotoTask(Context context, PhotoBookDatabase database, Photo photo) {
            this.mContext = context;
            this.mDatabase = database;
            this.mPhoto = photo;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                PhotosDao photosDao = mDatabase.photosDao();
                photosDao.update(mPhoto);

                return true;
            } catch (Exception e) {
                e.printStackTrace();

                Timber.d("doInBackground: " + e.toString());

                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {
                Timber.d("onPostExecute: " + mPhoto.toString());
            }
        }
    }

    private static class GetPhotoBookmarkTask extends AsyncTask<Void, Void, List<Photo>> {
        private final MutableLiveData<List<Photo>> mMutableLiveData;
        private Context mContext;
        private PhotoBookDatabase mDatabase;
        private Photo mPhoto;

        public GetPhotoBookmarkTask(Context context, PhotoBookDatabase database, MutableLiveData<List<Photo>> mutableLiveData) {
            this.mContext = context;
            this.mDatabase = database;
            this.mMutableLiveData = mutableLiveData;
        }

        @Override
        protected List<Photo> doInBackground(Void... params) {
            try {
                return mDatabase.photosDao().getBookMark();
            } catch (Exception e) {
                e.printStackTrace();

                Timber.d("doInBackground: " + e.toString());

                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Photo> photoList) {
            super.onPostExecute(photoList);
            mMutableLiveData.setValue(photoList);
        }
    }
}
