package com.puthykuch.photobook;

import android.app.Application;

import com.puthykuch.photobook.ui.di.components.ApplicationComponent;
import com.puthykuch.photobook.ui.di.components.DaggerApplicationComponent;

import timber.log.Timber;

public class PhotoBookApplication extends Application {

    ApplicationComponent applicationComponent;

    public ApplicationComponent getApplicationComponent() {
        if(applicationComponent == null) {
            applicationComponent = DaggerApplicationComponent.builder().build();
        }

        return applicationComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // init Timber for logging
        Timber.plant(new Timber.DebugTree());
    }

}
