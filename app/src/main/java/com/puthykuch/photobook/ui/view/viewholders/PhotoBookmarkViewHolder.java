package com.puthykuch.photobook.ui.view.viewholders;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.ybq.android.spinkit.SpinKitView;
import com.puthykuch.photobook.R;
import com.puthykuch.photobook.service.respository.storage.models.Photo;

public class PhotoBookmarkViewHolder extends RecyclerView.ViewHolder {

    private Context mContext;
    private OnPhotoBookmarkClickListener mOnPhotoClickListener;

    private CardView cardview;
    private SpinKitView spinKitView;
    private TextView tvAuthor, tvPhotoRemoveFromBookmark;
    private ImageView ivPhoto;

    public PhotoBookmarkViewHolder(Context context, View itemView, OnPhotoBookmarkClickListener onPhotobookmarkClickListener) {
        super(itemView);

        mContext = context;
        mOnPhotoClickListener = onPhotobookmarkClickListener;

        cardview = itemView.findViewById(R.id.cardview);
        spinKitView = itemView.findViewById(R.id.spin_kit);
        tvAuthor = itemView.findViewById(R.id.tvAuthor);
        ivPhoto = itemView.findViewById(R.id.ivPhoto);
        tvPhotoRemoveFromBookmark = itemView.findViewById(R.id.tvRemoveFromBookMark);
    }

    public void bindTo(Photo photo, int position) {
        spinKitView.setVisibility(View.VISIBLE);

        // bind data with view
        tvAuthor.setText(photo.getAuthor());

        Glide.with(mContext)
                .load(photo.getDownloadUrl())
                .error(Glide.with(mContext)
                        .load(R.drawable.no_internet_connection))
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        spinKitView.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        spinKitView.setVisibility(View.GONE);
                        return false;
                    }
                })
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivPhoto);

        // set event
        cardview.setOnClickListener(v -> mOnPhotoClickListener.listItemClick(photo));
        tvPhotoRemoveFromBookmark.setOnClickListener(v -> mOnPhotoClickListener.removeBookMarkClick(photo, position));
    }

    public interface OnPhotoBookmarkClickListener {
        void listItemClick(Photo item);

        void removeBookMarkClick(Photo item, int position);
    }
}

