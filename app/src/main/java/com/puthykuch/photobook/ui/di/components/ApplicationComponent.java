package com.puthykuch.photobook.ui.di.components;

import com.puthykuch.photobook.ui.di.modules.ActivityModule;
import com.puthykuch.photobook.ui.di.qualifiers.ApplicationContext;
import com.puthykuch.photobook.ui.di.modules.ApplicationModule;

import dagger.Component;

@ApplicationContext
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    ActivityComponent activityComponent(ActivityModule module);

}



