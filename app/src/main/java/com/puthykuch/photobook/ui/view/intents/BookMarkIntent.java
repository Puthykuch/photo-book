package com.puthykuch.photobook.ui.view.intents;

import android.content.Context;
import android.content.Intent;

import com.puthykuch.photobook.ui.view.activity.bookmark.BookMarkActivity;

public class BookMarkIntent extends Intent {
    public BookMarkIntent(Context context) {
        super(context, BookMarkActivity.class);
    }
}
