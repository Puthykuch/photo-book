package com.puthykuch.photobook.ui.adapter.paging;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.puthykuch.photobook.R;
import com.puthykuch.photobook.service.respository.storage.models.NetworkState;
import com.puthykuch.photobook.service.respository.storage.models.Photo;
import com.puthykuch.photobook.ui.view.viewholders.NetworkStateItemViewHolder;
import com.puthykuch.photobook.ui.view.viewholders.PhotoViewHolder;

public class PhotoPageListAdapter extends PagedListAdapter<Photo, RecyclerView.ViewHolder> {

    private final Context mContext;
    private NetworkState networkState;
    private PhotoViewHolder.OnPhotoClickListener mOnPhotoClickListener;

    public PhotoPageListAdapter(Context context, PhotoViewHolder.OnPhotoClickListener onPhotoClickListener) {
        super(Photo.DIFF_CALLBACK);
        this.mContext = context;
        this.mOnPhotoClickListener = onPhotoClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        if (viewType == R.layout.photo_list_item) {
            View view = layoutInflater.inflate(R.layout.photo_list_item, parent, false);
            PhotoViewHolder viewHolder = new PhotoViewHolder(mContext, view, mOnPhotoClickListener);
            return viewHolder;
        } else if (viewType == R.layout.network_state_item) {
            View view = layoutInflater.inflate(R.layout.network_state_item, parent, false);
            return new NetworkStateItemViewHolder(view);
        } else {
            throw new IllegalArgumentException("unknown view type");
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case R.layout.photo_list_item:
                ((PhotoViewHolder) holder).bindTo(getItem(position), position);
                break;
            case R.layout.network_state_item:
                ((NetworkStateItemViewHolder) holder).bindView(networkState);
                break;
        }
    }

    private boolean hasExtraRow() {
        if (networkState != null && networkState != NetworkState.LOADED) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (hasExtraRow() && position == getItemCount() - 1) {
            return R.layout.network_state_item;
        } else {
            return R.layout.photo_list_item;
        }
    }

    public void setNetworkState(NetworkState newNetworkState) {
        NetworkState previousState = this.networkState;
        boolean previousExtraRow = hasExtraRow();
        this.networkState = newNetworkState;
        boolean newExtraRow = hasExtraRow();
        if (previousExtraRow != newExtraRow) {
            if (previousExtraRow) {
                notifyItemRemoved(getItemCount());
            } else {
                notifyItemInserted(getItemCount());
            }
        } else if (newExtraRow && previousState != newNetworkState) {
            notifyItemChanged(getItemCount() - 1);
        }
    }
}
