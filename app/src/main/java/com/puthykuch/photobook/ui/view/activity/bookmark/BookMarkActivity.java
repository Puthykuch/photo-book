package com.puthykuch.photobook.ui.view.activity.bookmark;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.ActionBar;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.puthykuch.photobook.R;
import com.puthykuch.photobook.databinding.ActivityBookMarkBinding;
import com.puthykuch.photobook.service.respository.network.ApiService;
import com.puthykuch.photobook.service.respository.storage.models.Photo;
import com.puthykuch.photobook.ui.adapter.paging.PhotoBookmarkListAdapter;
import com.puthykuch.photobook.ui.adapter.paging.PhotoPageListAdapter;
import com.puthykuch.photobook.ui.di.qualifiers.ActivityContext;
import com.puthykuch.photobook.ui.view.activity.base.BindingActivity;
import com.puthykuch.photobook.ui.view.intents.FullImageIntent;
import com.puthykuch.photobook.ui.view.viewholders.PhotoBookmarkViewHolder;
import com.puthykuch.photobook.ui.viewmodels.PhotosListViewModel;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

public class BookMarkActivity extends BindingActivity<ActivityBookMarkBinding> implements IBookMarkAcvity, PhotoBookmarkViewHolder.OnPhotoBookmarkClickListener {

    @Inject
    @ActivityContext
    ApiService mApiService;

    @Inject
    @ActivityContext
    Context mContext;

    PhotosListViewModel viewModel;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_book_mark;
    }

    @Override
    public boolean hideStatusBar() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        changeStatusBarColor(R.color.colorAccent);

        super.onCreate(savedInstanceState);

        getActivityComponent().inject(this);

        viewModel = ViewModelProviders.of(this).get(PhotosListViewModel.class);

        viewModel.init(mContext, mApiService);

        setupToolbar();

        setRecyclerView();

        observerRegisters();
    }

    public void changeStatusBarColor(int resourseColor) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), resourseColor));
        }
    }

    private void setupToolbar() {
        setSupportActionBar(mBinding.toolbar);
        mBinding.toolbar.setTitle(this.getTitle());
        mBinding.toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
    }

    private void setRecyclerView() {
        if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            mBinding.rvPhotoList.setLayoutManager(new LinearLayoutManager(this));
        } else {
            mBinding.rvPhotoList.setLayoutManager(new GridLayoutManager(mContext, 3));
        }
    }

    @Override
    public void observerRegisters() {
        PhotoBookmarkListAdapter photoBookmarkListAdapter = new PhotoBookmarkListAdapter(mContext, this);
        mBinding.rvPhotoList.setAdapter(photoBookmarkListAdapter);
        viewModel.getBookMarks().observe(this, (List<Photo> photoBookmark) -> {
            if(photoBookmark.size() <= 0) {
                mBinding.tvMessage.setVisibility(View.VISIBLE);
            } else {
                mBinding.tvMessage.setVisibility(View.GONE);
            }

            photoBookmarkListAdapter.setPhotoBookmark(photoBookmark);
        });
    }

    @Override
    public void listItemClick(Photo photo) {
        FullImageIntent fullImageIntent = new FullImageIntent(mContext);
        fullImageIntent.setPhoto(photo);
        startActivity(fullImageIntent);
    }

    @Override
    public void removeBookMarkClick(Photo photo, int position) {
        alertAction("You want to remove this bookmark?", (dialog, which) -> {
            viewModel.removeBookMark(photo);
            observerRegisters();
        });
    }
}
