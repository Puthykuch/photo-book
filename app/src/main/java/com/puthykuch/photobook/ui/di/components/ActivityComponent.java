package com.puthykuch.photobook.ui.di.components;

import com.puthykuch.photobook.ui.di.qualifiers.ActivityContext;
import com.puthykuch.photobook.ui.di.modules.ActivityModule;
import com.puthykuch.photobook.ui.view.activity.bookmark.BookMarkActivity;
import com.puthykuch.photobook.ui.view.activity.photodetails.PhotoDetailsActivity;
import com.puthykuch.photobook.ui.view.activity.main.MainActivity;

import dagger.Subcomponent;

@Subcomponent(modules = {ActivityModule.class})
@ActivityContext
public interface ActivityComponent {

    void inject(MainActivity listPhotoActivity);

    void inject(BookMarkActivity bookMarkActivity);

    void inject(PhotoDetailsActivity photoDetailsActivity);

}
