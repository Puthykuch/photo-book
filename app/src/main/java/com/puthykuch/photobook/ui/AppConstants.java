package com.puthykuch.photobook.ui;

import com.puthykuch.photobook.BuildConfig;

public interface AppConstants {

    // network
    String BASE_URL = "https://picsum.photos/";

    int LOADING_PAGE_SIZE = 15;

    long HTTP_CACHE_SIZE = 10 * 1024 * 1024;

    int API_CONNECT_TIMEOUT = BuildConfig.DEBUG ? 1 : 5;

    int API_READ_TIMEOUT = BuildConfig.DEBUG ? 1 : 5;

    // database
    String DATA_BASE_NAME = "photobook.db";
    int NUMBERS_OF_THREADS = 3;
}
