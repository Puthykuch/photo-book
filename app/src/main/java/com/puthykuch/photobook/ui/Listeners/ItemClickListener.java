package com.puthykuch.photobook.ui.Listeners;

import com.puthykuch.photobook.service.respository.storage.models.Photo;

public interface ItemClickListener {
    void OnItemClick(Photo photo);
}
