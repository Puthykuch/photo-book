package com.puthykuch.photobook.ui.adapter.paging;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.puthykuch.photobook.R;
import com.puthykuch.photobook.service.respository.storage.models.Photo;
import com.puthykuch.photobook.ui.view.viewholders.PhotoBookmarkViewHolder;

import java.util.ArrayList;
import java.util.List;

public class PhotoBookmarkListAdapter extends RecyclerView.Adapter<PhotoBookmarkViewHolder> {

    private Context mContext;
    private PhotoBookmarkViewHolder.OnPhotoBookmarkClickListener onPhotoBookmarkClickListener;

    private List<Photo> mPhotoBookmark = new ArrayList<>();

    public PhotoBookmarkListAdapter(Context context, PhotoBookmarkViewHolder.OnPhotoBookmarkClickListener onPhotobookmarkClickListener) {
        mContext = context;
        onPhotoBookmarkClickListener = onPhotobookmarkClickListener;
    }

    @NonNull
    @Override
    public PhotoBookmarkViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.bookmark_list_item, parent, false);
        PhotoBookmarkViewHolder viewHolder = new PhotoBookmarkViewHolder(mContext, view, onPhotoBookmarkClickListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoBookmarkViewHolder holder, int position) {
        holder.bindTo(mPhotoBookmark.get(position), position);
    }

    @Override
    public int getItemCount() {
        return mPhotoBookmark.size();
    }

    public void setPhotoBookmark(List<Photo> photoBookmark) {
        mPhotoBookmark = photoBookmark;
    }
}
