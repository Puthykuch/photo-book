package com.puthykuch.photobook.ui.view.activity.main;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.IntentCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.puthykuch.photobook.R;
import com.puthykuch.photobook.databinding.ActivityMainBinding;
import com.puthykuch.photobook.service.respository.network.ApiService;
import com.puthykuch.photobook.service.respository.storage.models.Photo;
import com.puthykuch.photobook.ui.adapter.paging.PhotoPageListAdapter;
import com.puthykuch.photobook.ui.di.qualifiers.ActivityContext;
import com.puthykuch.photobook.ui.view.activity.base.BindingActivity;
import com.puthykuch.photobook.ui.view.intents.BookMarkIntent;
import com.puthykuch.photobook.ui.view.intents.FullImageIntent;
import com.puthykuch.photobook.ui.view.intents.MainIntent;
import com.puthykuch.photobook.ui.view.viewholders.PhotoViewHolder;
import com.puthykuch.photobook.ui.viewmodels.PhotosListViewModel;

import java.util.Objects;

import javax.inject.Inject;

public class MainActivity extends BindingActivity<ActivityMainBinding> implements IMainActivity, PhotoViewHolder.OnPhotoClickListener {

    @Inject
    @ActivityContext
    ApiService mApiService;

    @Inject
    @ActivityContext
    Context mContext;

    PhotosListViewModel viewModel;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    public boolean hideStatusBar() {
        return false;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(this);

        viewModel = ViewModelProviders.of(this).get(PhotosListViewModel.class);
        viewModel.init(mContext, mApiService);

        setToolbar();

        setRecyclerView();

        setEventListener();

        showLoading(true);
        new Handler().postDelayed(() -> {
            showLoading(false);
        }, 1200);
    }

    private void setToolbar() {
        setSupportActionBar(mBinding.toolbar);
        mBinding.toolbar.setTitle(R.string.app_name);
        mBinding.toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
    }

    private void setRecyclerView() {
        if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            mBinding.rvPhotoList.setLayoutManager(new LinearLayoutManager(this));
        } else {
            mBinding.rvPhotoList.setLayoutManager(new GridLayoutManager(mContext, 3));
        }

        mBinding.rvPhotoList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && mBinding.fabBookMark.getVisibility() == View.VISIBLE) {
                    mBinding.fabBookMark.hide();
                } else if (dy < 0 && mBinding.fabBookMark.getVisibility() != View.VISIBLE) {
                    mBinding.fabBookMark.show();
                }
            }
        });
    }

    private void setEventListener() {
        mBinding.fabBookMark.setOnClickListener(v -> onOpenBookMarksClick());
    }

    @Override
    public void observerRegisters() {
        PhotoPageListAdapter pageListAdapter = new PhotoPageListAdapter(mContext, this);
        viewModel.getPhotos().observe(this, pageListAdapter::submitList);
        viewModel.getNetworkState().observe(this, pageListAdapter::setNetworkState);
        mBinding.rvPhotoList.setItemAnimator(null);
        mBinding.rvPhotoList.setAdapter(pageListAdapter);
    }

    @Override
    public void onOpenBookMarksClick() {
        BookMarkIntent bookMarkIntent = new BookMarkIntent(mContext);
        startActivity(bookMarkIntent);
    }

    @Override
    public void listItemClick(Photo photo) {
        FullImageIntent fullImageIntent = new FullImageIntent(mContext);
        fullImageIntent.setPhoto(photo);
        startActivity(fullImageIntent);
    }


    @Override
    public void bookMarkClick(Photo photo, int position) {
        viewModel.toggleBookMark(photo);
        mBinding.rvPhotoList.getAdapter().notifyItemChanged(position);
    }

    @Override
    protected void onResume() {
        super.onResume();

        observerRegisters();
    }

    @Override
    public FloatingActionButton getFab() {
        return mBinding.fabBookMark;
    }
}
