package com.puthykuch.photobook.ui.view.intents;

import android.content.Context;
import android.content.Intent;

import com.puthykuch.photobook.service.respository.storage.models.Photo;
import com.puthykuch.photobook.ui.view.activity.photodetails.PhotoDetailsActivity;

public class FullImageIntent extends Intent {

    private static String PHOTO_EXTRA = "PHOTO_EXTRA";

    public FullImageIntent(Context context) {
        super(context, PhotoDetailsActivity.class);
    }

    public void setPhoto(Photo photo) {
        putExtra(PHOTO_EXTRA, photo);
    }

    public static Photo getPhoto(PhotoDetailsActivity context) {
        return context.getIntent().getParcelableExtra(PHOTO_EXTRA);
    }

}
