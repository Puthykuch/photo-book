package com.puthykuch.photobook.ui.view.viewholders;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.ybq.android.spinkit.SpinKitView;
import com.puthykuch.photobook.R;
import com.puthykuch.photobook.service.respository.storage.models.Photo;

public class PhotoViewHolder extends RecyclerView.ViewHolder {

    private Context mContext;
    private OnPhotoClickListener mOnPhotoClickListener;

    private CardView cardview;
    private SpinKitView spinKitView;
    private TextView tvAuthor, tvAddBookMark;
    private ImageView ivPhoto;

    public PhotoViewHolder(Context context, View itemView, OnPhotoClickListener onPhotoClickListener) {
        super(itemView);

        mContext = context;
        mOnPhotoClickListener = onPhotoClickListener;

        cardview = itemView.findViewById(R.id.cardview);
        spinKitView = itemView.findViewById(R.id.spin_kit);
        tvAuthor = itemView.findViewById(R.id.tvAuthor);
        ivPhoto = itemView.findViewById(R.id.ivPhoto);
        tvAddBookMark = itemView.findViewById(R.id.tvAddBookMark);
    }

    public void bindTo(Photo photo, int position) {
        spinKitView.setVisibility(View.VISIBLE);

        // bind data with view
        tvAuthor.setText(photo.getAuthor());

        Glide.with(mContext)
                .load(photo.getDownloadUrl())
                .error(Glide.with(mContext)
                        .load(R.drawable.no_internet_connection))
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        spinKitView.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        spinKitView.setVisibility(View.GONE);
                        return false;
                    }
                })
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivPhoto);

        if (photo.getMark()) {
            tvAddBookMark.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
            tvAddBookMark.setCompoundDrawablesWithIntrinsicBounds(null, mContext.getDrawable(R.drawable.ic_bookmark_primary), null, null);
        } else {
            tvAddBookMark.setTextColor(mContext.getResources().getColor(R.color.colorSilver));
            tvAddBookMark.setCompoundDrawablesWithIntrinsicBounds(null, mContext.getDrawable(R.drawable.ic_bookmark_silver), null, null);
        }

        // set event
        cardview.setOnClickListener(v -> mOnPhotoClickListener.listItemClick(photo));
        tvAddBookMark.setOnClickListener(v -> mOnPhotoClickListener.bookMarkClick(photo, position));
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public interface OnPhotoClickListener {
        void listItemClick(Photo item);

        void bookMarkClick(Photo item, int position);
    }
}

