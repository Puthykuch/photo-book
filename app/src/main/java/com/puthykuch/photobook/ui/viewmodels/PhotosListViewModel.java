package com.puthykuch.photobook.ui.viewmodels;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.paging.PagedList;

import com.puthykuch.photobook.service.respository.PhotosRepository;
import com.puthykuch.photobook.service.respository.network.ApiService;
import com.puthykuch.photobook.service.respository.storage.models.NetworkState;
import com.puthykuch.photobook.service.respository.storage.models.Photo;

import java.util.List;

public class PhotosListViewModel extends AndroidViewModel {
    private PhotosRepository repository;

    public PhotosListViewModel(@NonNull Application application) {
        super(application);
    }

    public void init(Context context, ApiService apiService) {
        if (repository != null) {
            return;
        }

        repository = PhotosRepository.getInstance(context, apiService);
    }

    public LiveData<PagedList<Photo>> getPhotos() {
        return repository.getPhotos();
    }

    public LiveData<List<Photo>> getBookMarks() {
        return repository.getBookMarks();
    }

    public LiveData<NetworkState> getNetworkState() {
        return repository.getNetworkState();
    }

    public void toggleBookMark(Photo photo) {
        repository.toggleBookMark(photo);
    }

    public void removeBookMark(Photo photo) {
        repository.removeBookMark(photo);
    }
}
