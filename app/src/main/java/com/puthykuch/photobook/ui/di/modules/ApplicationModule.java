package com.puthykuch.photobook.ui.di.modules;

import android.app.Application;

import com.puthykuch.photobook.ui.di.qualifiers.ApplicationContext;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    @ApplicationContext
    public Application provideApplication() {
        return mApplication;
    }
}
