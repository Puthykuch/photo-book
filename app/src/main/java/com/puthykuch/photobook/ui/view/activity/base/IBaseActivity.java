package com.puthykuch.photobook.ui.view.activity.base;

import android.content.Context;
import android.content.DialogInterface;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public interface IBaseActivity {

    Context getContext();

    void alertErrorMessage(String message);

    void alertAction(String message, DialogInterface.OnClickListener onClickListener);

    void showLoading(boolean show);

    void showNotification(String message, FloatingActionButton fab);

    void validateNetwork();
}
