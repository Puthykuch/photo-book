package com.puthykuch.photobook.ui.di.modules;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;
import com.puthykuch.photobook.service.respository.network.ApiService;
import com.puthykuch.photobook.ui.di.qualifiers.ActivityContext;
import com.puthykuch.photobook.ui.AppConstants;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

@Module
public class NetworkModule implements AppConstants {

    @Provides
    @ActivityContext
    public String provideUrl() {
        return AppConstants.BASE_URL;
    }

    @Provides
    @ActivityContext
    public Cache provideCache(Context context) {
        return new Cache(context.getCacheDir(), AppConstants.HTTP_CACHE_SIZE);
    }

    @Provides
    @ActivityContext
    public ApiService provideService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @ActivityContext
    public Retrofit provideServerRetrofit(Retrofit.Builder builder, String url) {
        return builder.baseUrl(url).build();
    }

    @Provides
    @ActivityContext
    public Gson provideGson() {
        return new Gson();
    }

    @Provides
    @ActivityContext
    public Retrofit.Builder provideRetrofitBuilder(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson));
    }

    @Provides
    @ActivityContext
    public OkHttpClient provideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, Cache cache) {
        return new OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(httpLoggingInterceptor)
                .readTimeout(AppConstants.API_READ_TIMEOUT, TimeUnit.MINUTES)
                .connectTimeout(AppConstants.API_CONNECT_TIMEOUT, TimeUnit.MINUTES)
                .build();
    }

    @Provides
    @ActivityContext
    public HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLogger = new HttpLoggingInterceptor(message -> Timber.i(message));
        httpLogger.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLogger;
    }
}
