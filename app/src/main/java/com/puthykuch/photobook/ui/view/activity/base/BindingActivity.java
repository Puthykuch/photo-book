package com.puthykuch.photobook.ui.view.activity.base;

import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public abstract class BindingActivity<T extends ViewDataBinding> extends BaseActivity {

    protected T mBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(hideStatusBar()) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        mBinding = DataBindingUtil.setContentView(this, getLayoutResource());

        validateNetwork();
    }

    @LayoutRes
    protected abstract int getLayoutResource();

    @Override
    public FloatingActionButton getFab() {
        return null;
    }

    public abstract boolean hideStatusBar();
}
