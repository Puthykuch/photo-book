package com.puthykuch.photobook.ui.view.activity.photodetails;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.puthykuch.photobook.R;
import com.puthykuch.photobook.databinding.ActivityFullImageBinding;
import com.puthykuch.photobook.service.respository.storage.models.Photo;
import com.puthykuch.photobook.ui.di.qualifiers.ActivityContext;
import com.puthykuch.photobook.ui.view.intents.FullImageIntent;
import com.puthykuch.photobook.ui.view.activity.base.BindingActivity;
import com.puthykuch.photobook.ui.AppConstants;

import javax.inject.Inject;

public class PhotoDetailsActivity extends BindingActivity<ActivityFullImageBinding> implements IPhotoDetailsActivity {

    @Inject
    @ActivityContext
    Context mContext;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_full_image;
    }

    @Override
    public boolean hideStatusBar() {
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivityComponent().inject(this);

        Photo photo = FullImageIntent.getPhoto(this);

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;

        mBinding.tvAuthor.setText(photo.getAuthor());

        String imageUrl = AppConstants.BASE_URL + "id/" + photo.getId() + "/" + width + "/" + height;
        Glide.with(mContext)
                .load((imageUrl))
                .error(
                        Glide.with(mContext).load(R.drawable.no_internet_connection)
                )
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        mBinding.spinKit.setVisibility(View.GONE);
                        mBinding.iv.setImageDrawable(getDrawable(R.drawable.no_internet_connection));
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        mBinding.spinKit.setVisibility(View.GONE);
                        return false;
                    }
                })
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(mBinding.iv);
    }
}
