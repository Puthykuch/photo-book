package com.puthykuch.photobook.ui.view.activity.main;

import com.puthykuch.photobook.ui.view.activity.base.IBaseActivity;

interface IMainActivity extends IBaseActivity {

    void observerRegisters();

    void onOpenBookMarksClick();

}
