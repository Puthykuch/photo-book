package com.puthykuch.photobook.ui.view.activity.base;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.puthykuch.photobook.PhotoBookApplication;
import com.puthykuch.photobook.R;
import com.puthykuch.photobook.ui.di.components.ActivityComponent;
import com.puthykuch.photobook.ui.di.modules.ActivityModule;

public abstract class BaseActivity extends AppCompatActivity implements IBaseActivity {

    private ActivityComponent activityComponent;

    private Dialog dialog;

    public ActivityComponent getActivityComponent() {
        activityComponent = ((PhotoBookApplication) getApplication())
                .getApplicationComponent()
                .activityComponent(new ActivityModule(this));

        return activityComponent;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void alertErrorMessage(String message) {
        new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton("Ok", null)
                .show();
    }

    @Override
    public void showLoading(boolean show) {
        if (dialog == null) {
            dialog = new Dialog(getContext());
            dialog.setContentView(View.inflate(getContext(), R.layout.layout_loading, null));
            dialog.setCancelable(false);
        }

        if (show) {
            dialog.show();
        } else {
            dialog.dismiss();
        }
    }

    @Override
    public void showNotification(String message, FloatingActionButton fab) {
        if (fab != null) {
            Snackbar snackbar = Snackbar.make(fab, message, Snackbar.LENGTH_LONG);
            snackbar.setAction("Ok", new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            snackbar.show();

        } else {
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
            snackbar.setAction("Ok", new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            snackbar.show();
        }
    }

    @Override
    public void alertAction(String message, DialogInterface.OnClickListener onClickListener) {

        new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton(getString(R.string.confirm), onClickListener)
                .setNegativeButton(getString(R.string.cancel), null)
                .show();
    }

    @Override
    public void validateNetwork() {
        if (!isNetworkAvailable()) {
            showNotification("Application in offline mode!", getFab());
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public abstract FloatingActionButton getFab();
}
