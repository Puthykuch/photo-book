package com.puthykuch.photobook.ui.di.modules;

import android.content.Context;

import com.puthykuch.photobook.ui.di.qualifiers.ActivityContext;

import dagger.Module;
import dagger.Provides;

@Module(includes = NetworkModule.class)
public class ActivityModule {
    private final Context mContext;

    public ActivityModule(Context context) {
        mContext = context;
    }

    @Provides
    @ActivityContext
    public Context provideContext() {
        return mContext;
    }
}
