package com.puthykuch.photobook.ui.view.intents;

import android.content.Context;
import android.content.Intent;

import com.puthykuch.photobook.ui.view.activity.bookmark.BookMarkActivity;
import com.puthykuch.photobook.ui.view.activity.main.MainActivity;

public class MainIntent extends Intent {
    public MainIntent(Context context) {
        super(context, MainActivity.class);
    }
}
